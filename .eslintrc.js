/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution")

module.exports = {
	root: true,
	extends: [
		"plugin:vue/vue3-essential",
		"eslint:recommended",
	],
	env: {
		"vue/setup-compiler-macros": true,
	},
	rules: {
		indent: ["error", "tab"],
		"no-mixed-spaces-and-tabs": ["error", "smart-tabs"],
		quotes: ["error", "double"],
		"no-unused-vars": ["error", {"argsIgnorePattern": "^_"}],
		"comma-dangle": ["error", "always-multiline"],
		"semi": ["error", "never"],
		"no-console": "error",
	},
}
