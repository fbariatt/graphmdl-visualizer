/**
 * Utility functions used when specifying subsets on the rewritten graph visualization.
 *
 * A subset object must have the following form:
 * {
 *		"name": String, // Short name, to be shown in list.
 *		"description": String, // Optional. Longer description.
 *		// All following fields are optional. Each one that is specified makes the subset more specific.
 *		// For compatibility reasons, they can also be specified as fields of an object "limit".
 *		// Note that data vertices start at 0.
 *		"from": Number, // Only data vertices with number equal or higher than this value are accepted.
 *		"to": Number, // Only data vertices with number equal or lower than this value are accepted.
 *		"whitelist": [Number], // Only the specified data vertices are accepted.
 *	}
 */
export default {
	/**
	 * @return a subset object that does not put any limit on the data
	 */
	createAcceptAllSubsetObject: function() {
		return {
			name: "All data",
		}
	},
	/**
	 * @param subsetObject An object describing a data subset (see module documentation).
	 * @return {function(Number): Boolean} A filter function that can be applied to a data vertex to determine whether it belongs to the subset.
	 */
	subsetObjectToFilterFunction: function(subsetObject) {
		// Compatibility with the older form
		if (Object.prototype.hasOwnProperty.call(subsetObject, "limits"))
			subsetObject = subsetObject["limits"]

		let filters = []

		if (Object.prototype.hasOwnProperty.call(subsetObject, "from"))
			filters.push(dataVertex => dataVertex >= subsetObject["from"])
		if (Object.prototype.hasOwnProperty.call(subsetObject, "to"))
			filters.push(dataVertex => dataVertex <= subsetObject["to"])
		if (Object.prototype.hasOwnProperty.call(subsetObject, "whitelist"))
			filters.push(dataVertex => Array.prototype.includes.call(subsetObject["whitelist"], dataVertex))

		return dataVertex => filters.every(filter => filter(dataVertex))
	},
}
