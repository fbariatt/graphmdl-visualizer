import * as d3 from "d3"
import {Vector2} from "./Vector2"

/**
 * Utility functions for dealing with d3js
 */
export default {
	/**
	 * Increment the alpha coefficient of a force simulation and restart it.
	 * This "reignites" the simulation by injecting some energy into it, giving it a "kick".
	 */
	reIgniteSimulation: function (forceSimulation) {
		forceSimulation
			.alpha(Math.min(forceSimulation.alpha() + 0.3, 1)) // Adding more force, but without exaggerating
			.restart() // Making sure the simulation is running
	},
	/**
	 * @return A drag behaviour that allows to click and drag simulation nodes.
	 */
	forceSimulationDragBehaviour: function (forceSimulation) {
		return d3.drag()
			.on("start", (event, d) => {
				// Increase the simulation alpha target, so that it always keeps a little bit of energy
				// It makes the simulation "giggly" during the drag, making it more responsive to the drag
				// Also it makes it so that it does not settle down until the end of the drag (when we reset the target to 0)
				forceSimulation.alphaTarget(0.3)
				this.reIgniteSimulation(forceSimulation)

				// Make the selected node fixed, so that it won't move away from under the mouse
				d.fx = d.x
				d.fy = d.y
			})
			.on("drag", (event, d) => {
				// Move the selected node to the mouse position, making it fixed so that it won't move away
				// This make the selected node follow the mouse during the drag
				d.fx = event.x
				d.fy = event.y
			})
			.on("end", (event, d) => {
				// The vertex position is no longer fixed
				d.fx = null
				d.fy = null

				// Tell the simulation to settle down once the drag has ended
				forceSimulation.alphaTarget(0)
				this.reIgniteSimulation(forceSimulation)
			})
	},

	/**
	 * Generate the 'd' attribute of an svg path for drawing the line representing an edge.
	 * @param edge The edge for which the line should be drawn. Should have a source and a target both with x and y properties,
	 * and properties telling how many edges share the same pair of vertices and the index of this particular vertex between them.
	 * @param zoomTransform The current zoom of the graph plot
	 * @param baseLineSpread The base value (for a non-zoomed drawing) of the distance over which edges between the same
	 * pair of vertices can be spread.
	 * @return A string to be used as the 'd' attribute of a path.
	 */
	edgePath: function(edge, zoomTransform, baseLineSpread) {
		const source = new Vector2(zoomTransform.applyX(edge.source.x), zoomTransform.applyY(edge.source.y))
		const target = new Vector2(zoomTransform.applyX(edge.target.x), zoomTransform.applyY(edge.target.y))

		if (edge.totalWithSameEnds === 1) { // It is the only edge between the source and the target
			// We trace a simple line
			return `M ${source.x},${source.y} L ${target.x},${target.y}`
		} else { // There are multiple edges between the source and the target
			const lineSpread = zoomTransform.k * baseLineSpread
			const line = d3.line()
				.x(d => d.x)
				.y(d => d.y)
				.curve(d3.curveCardinal)
			return line([
				source,
				computeEdgeLineMidpoint(source, target, edge.totalWithSameEnds, edge.indexOfSameEnds, lineSpread),
				target,
			])
		}
	},

	/**
	 * Generate the 'd' attribute of an svg path for drawing the line representing the arrow indicating an edge's direction.
	 * @param edge The edge for which the line should be drawn, see {@link edgePath}
	 * @param zoomTransform The current zoom of the graph plot
	 * @param baseLineSpread The base value (for a non-zoomed drawing) of the distance over which edges between the same
	 * pair of vertices can be spread.
	 * @param vertexBaseRadius The base value (for a non-zoomed drawing) of a vertex radius
	 * @param arrowBaseSize The base value (for a non-zoomed drawing) for the size of the arrow that will be generated
	 * @return A string to be used as the 'd' attribute of a path.
	 */
	edgeArrowHeadPath: function(edge, zoomTransform, baseLineSpread, vertexBaseRadius, arrowBaseSize) {
		const source = new Vector2(zoomTransform.applyX(edge.source.x), zoomTransform.applyY(edge.source.y))
		const target = new Vector2(zoomTransform.applyX(edge.target.x), zoomTransform.applyY(edge.target.y))
		const edgeDirection = target.sub(source).normalized() // Normalized vector representing the edge line
		const perpendicularDirection = edgeDirection.perpendicular()
		const arrowSize = zoomTransform.k * arrowBaseSize

		let arrowMidPoint
		if (edge.totalWithSameEnds === 1) { // It is the only edge between the source and the target
			// Arrow will be placed at the end of the line
			const vertexRadius = zoomTransform.k * vertexBaseRadius
			arrowMidPoint = target.sub(edgeDirection.mul(vertexRadius + 5)) // The midpoint of the arrow will be tangent to the vertex circle (plus a margin)
		} else {
			// Arrow will be placed in the middle of the line
			const lineSpread = zoomTransform.k * baseLineSpread
			arrowMidPoint = computeEdgeLineMidpoint(source, target, edge.totalWithSameEnds, edge.indexOfSameEnds, lineSpread)
		}

		const arrowStart = arrowMidPoint.sub(edgeDirection.mul(arrowSize)).sub(perpendicularDirection.mul(arrowSize))
		const arrowEnd = arrowStart.add(perpendicularDirection.mul(2 * arrowSize))

		return `M ${arrowStart.x},${arrowStart.y} L ${arrowMidPoint.x} ${arrowMidPoint.y} L ${arrowEnd.x} ${arrowEnd.y}`
	},

	/**
	 * The position that an edge label should have to be on the edge line.
	 * See {@link edgePath}.
	 * @param edge The edge for which the line should be drawn, see {@link edgePath}
	 * @param zoomTransform The current zoom of the graph plot
	 * @param baseLineSpread The base value (for a non-zoomed drawing) of the distance over which edges between the same
	 * pair of vertices can be spread.
	 * @return The position of the edge label as a Vector2 object
	 */
	edgeLabelPosition: function(edge, zoomTransform, baseLineSpread) {
		const source = new Vector2(zoomTransform.applyX(edge.source.x), zoomTransform.applyY(edge.source.y))
		const target = new Vector2(zoomTransform.applyX(edge.target.x), zoomTransform.applyY(edge.target.y))
		const lineSpread = zoomTransform.k * baseLineSpread

		return computeEdgeLineMidpoint(source, target, edge.totalWithSameEnds, edge.indexOfSameEnds, lineSpread)
	},
}

/**
 * Compute the point through which an edge line should pass so that all edge lines between the same pair of vertices are spread out.
 * @param source The edge source, as a {@link Vector2}
 * @param target The edge source, as a {@link Vector2}
 * @param totalEdgesWithSameEnds The total number of edges between the source and the target
 * @param edgeIndex The index of this edge among all edges between the same source and target
 * @param lineSpread The distance over which edges between the same pair of vertices can be spread.
 * @return {Vector2}
 */
function computeEdgeLineMidpoint(source, target, totalEdgesWithSameEnds, edgeIndex, lineSpread) {
	if (totalEdgesWithSameEnds === 1) // If this is the only edge, it will pass through the midpoint between source and target
		return source.add(target.sub(source).mul(0.5))

	if (source.compareTo(target) > 0)
		[source, target] = [target, source]

	const edgeVector = target.sub(source)
	const perpendicularDirection = edgeVector.perpendicular().normalized()
	return source.add(edgeVector.mul(0.5)) // Midpoint
		.sub(perpendicularDirection.mul(lineSpread / 2)) // Leftmost point that we can pass through
		.add(perpendicularDirection.mul(lineSpread / (totalEdgesWithSameEnds - 1) * edgeIndex)) // Moving the line for each different edge
}
