export const GraphType = Object.freeze({
	SIMPLE_UNDIRECTED: 0, // ...00
	SIMPLE_DIRECTED: 1, // ...01
	MULTI_UNDIRECTED: 2, // ...10
	MULTI_DIRECTED: 3, // ...11

	isDirected: function(type) {
		return Boolean(type & 1)
	},

	isMultiGraph: function(type) {
		return Boolean(type & 2)
	},

	fromString: function(string) {
		switch (string) {
		case "simple_undirected":
			return this.SIMPLE_UNDIRECTED
		case "simple_directed":
			return this.SIMPLE_DIRECTED
		case "multi_undirected":
			return this.MULTI_UNDIRECTED
		case "multi_directed":
			return this.MULTI_DIRECTED
		default:
			throw new Error(`Impossible to parse graph type ${string}: unknown type`)
		}
	},
})
