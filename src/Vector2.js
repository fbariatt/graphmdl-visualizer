/**
 * Class for representing 2-dimensional vectors
 */
export class Vector2 {
	constructor(x, y) {
		this.x = x
		this.y = y
	}

	/**
	 * @return The norm of this vector
	 */
	get norm() {
		return Math.hypot(this.x, this.y)
	}

	/**
	 * @return A new vector, result of the addition of this vector and another
	 */
	add(vector) {
		return new Vector2(this.x + vector.x, this.y + vector.y)
	}

	/**
	 * @return A new vector, result of subtracting another vector from this one
	 */
	sub(vector) {
		return new Vector2(this.x - vector.x, this.y - vector.y)
	}

	/**
	 * @return A new vector, result of multiplying this vector by a scalar
	 */
	mul(scalar) {
		return new Vector2(this.x * scalar, this.y * scalar)
	}

	/**
	 * @return A new vector, normalized version of this one.
	 */
	normalized() {
		const norm = this.norm
		return new Vector2(this.x / norm, this.y / norm)
	}

	/**
	 * @return A new vector, which is the inverse of this one
	 */
	inverse() {
		return new Vector2(-this.x, -this.y)
	}

	/**
	 * @return A vector that is perpendicular to the current one in a Clockwise manner in svg, where y grows towards the bottom,
	 * and CounterClockwise on a classic plane, where y grows towards the top.
	 */
	perpendicular() {
		return new Vector2(-this.y, this.x)
	}

	/**
	 * Compare this vector to another.
	 * The comparison is first done on the x value, and then y value in case of equality.
	 * @return A number < 0 if this vector compares lower than the other, a number > 0 if it compares higher, and 0 if it compares equal.
	 */
	compareTo(vector) {
		const xCompare = this.x - vector.x
		if (xCompare !== 0)
			return xCompare
		else
			return this.y - vector.y
	}

	toString() {
		return `Vector2(${this.x}, ${this.y})`
	}
}
