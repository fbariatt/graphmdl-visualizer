import {createApp} from "vue"
import App from "./App.vue"
import SmartTable from "vuejs-smart-table"

const app = createApp(App)

app.use(SmartTable)

import "bootstrap-icons/font/bootstrap-icons.css"

app.mount("#app")
