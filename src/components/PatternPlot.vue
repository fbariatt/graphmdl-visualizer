<template>
<div>
	<div class="graph-controls">
		<div class="repulsion-control">
			<label>Vertex repulsion strength</label>
			<input type="number" v-model="vertexRepulsionStrength"/>
		</div>
		<div class="checkboxes">
			<div class="checkbox-control">
				<label for="PatternPlot.showVertexNumbers">Show vertex numbers</label>
				<input type="checkbox" v-model="showVertexNumbers" id="PatternPlot.showVertexNumbers"/>
			</div>
			<div class="checkbox-control">
				<label for="PatternPlot.showFullCircles">Show ports as full circles</label>
				<input type="checkbox" v-model="showPortsAsFullCircles" id="PatternPlot.showFullCircles">
			</div>
			<div class="checkbox-control">
				<label for="PatternPlot.showLabels">Show labels</label>
				<input type="checkbox" v-model="showLabels" id="PatternPlot.showLabels">
			</div>
		</div>
		<div class="zoom-controls">
			<button class="zoom-reset" @click="resetZoom()">Reset zoom</button>
		</div>
	</div>
	<svg ref="svg" :width="width" :height="height"></svg>
</div>
</template>

<script>
import * as d3 from "d3"
import PatternUtils from "../PatternUtils"
import D3Utils from "../D3Utils"
import {GraphType} from "../GraphType"

const VERTEX_CIRCLE_BASE_RADIUS = 5
const VERTEX_CIRCLE_BASE_STROKE_WIDTH = 2
const EDGE_LINE_BASE_STROKE_WIDTH = 2
const EDGE_LINES_BASE_SPREAD = 40
const EDGE_ARROW_BASE_SIZE = 5
const VERTEX_NUMBER_BASE_FONT_SIZE = 5
const LABELS_MAX_CHARS = 30
const LABELS_BASE_FONT_SIZE = Object.freeze({normal: 7, min: 10})

export default {
	name: "PatternPlot",
	props: {
		pattern: {
			type: Object,
			required: true,
		},
		graphType: {
			default: GraphType.SIMPLE_UNDIRECTED,
		},
		vertexColorFromLabels: { // Called to get the color of a vertex. Should expect an array of vertex labels
			type: Function,
			default: () => "steelblue",
		},
		edgeColorFromLabel: { // Called to get the color of an edge. Should expect a string
			type: Function,
			default: () => "#999",
		},
		width: {
			type: Number,
			default: 500,
		},
		height: {
			type: Number,
			default: 500,
		},
	},
	data() {
		return {
			vertexRepulsionStrength: 150,
			showVertexNumbers: false,
			forceSimulation: null,
			vertexGroup: null,
			vertexCircles: null,
			vertexNumbers: null,
			edgeLinesGroup: null,
			edgeLines: null,
			edgeArrowsGroup: null,
			edgeArrows: null,
			hoverTextGroup: null,
			showPortsAsFullCircles: false,
			showLabels: false,
			labelGroup: null,
			vertexLabelGroup: null,
			vertexLabels: null,
			edgeLabelGroup: null,
			edgeLabels: null,
		}
	},
	computed: {
		structure: function () {
			return this.pattern["structure"]
		},
		vertices: function () {
			return this.structure["vertices"].map((v,i) => ({labels: v, vertexNumber: i, isPort: PatternUtils.isPort(this.pattern, i)}))
		},
		edges: function () {
			const edges = this.structure.edges.map(edge => ({source: edge.source, target: edge.target, label: edge.label}))
			const edgesGroupedByEnds = PatternUtils.groupEdgesByEnds(edges)

			edgesGroupedByEnds.forEach(edgesWithSameEnds => {
				const count = edgesWithSameEnds.length
				edgesWithSameEnds.forEach((edge, i) => {
					edge.totalWithSameEnds = count // Each edge knows how many vertices are between the same pair of vertices
					edge.indexOfSameEnds = i // Index of this edge between all edges that have the same ends
				})
			})

			return edges
		},
		svg : function () { // d3 selection containing the graph
			return 	d3.select(this.$refs.svg)
		},
		vertexRepulsiveForce: function () {
			return d3.forceManyBody().strength(-this.vertexRepulsionStrength)
		},
		linkForce: function () {
			return d3.forceLink(this.edges).distance(40)
		},
		zoomBehaviour: function () {
			return d3.zoom()
				.scaleExtent([0.2, 20])
				.on("zoom", this.plotChangingAttributes)
		},
	},
	methods: {
		plot: function () {
			if (!this.edgeLinesGroup)
				this.edgeLinesGroup = this.svg.append("g") // Group containing edge lines. Defined *first* so that edges will be *under* the vertices
			if (!this.vertexGroup)
				this.vertexGroup = this.svg.append("g") // Group containing all vertex-related elements

			if (this.forceSimulation) // If there is a simulation already running, we make sure to stop it
				this.forceSimulation.stop()
			this.forceSimulation = d3.forceSimulation(this.vertices)
				.force("vertexRepulsive", this.vertexRepulsiveForce)
				.force("link", this.linkForce)
			// Since the graph is possibly disjoint, we use forceX and forceY instead of forceCenter to prevent detached subgraphs from escaping the viewport.
				.force("x", d3.forceX(this.width / 2))
				.force("y", d3.forceY(this.height / 2))

			this.vertexCircles = this.vertexGroup.selectAll("circle")
				.data(this.vertices)
				.join("circle")
			this.colorVertices()

			this.vertexNumbers = this.vertexGroup.selectAll("text")
				.data(this.vertices)
				.join("text")
				.text((_, i) => i)
				.attr("text-anchor", "middle")
				.attr("cursor", "default")

			// Edges lines
			this.edgeLines = this.edgeLinesGroup.selectAll("path")
				.data(this.edges)
				.join("path")
				.attr("fill-opacity", 0)

			// Edge arrow heads if graph is directed
			if (!this.edgeArrowsGroup)
				this.edgeArrowsGroup = this.svg.append("g")
			if (GraphType.isDirected(this.graphType)) {
				this.edgeArrows = this.edgeArrowsGroup.selectAll("path")
					.data(this.edges)
					.join("path")
					.attr("fill-opacity", 0)
			} else {
				if (this.edgeArrows !== null)
					this.edgeArrows.remove()
				this.edgeArrows = null
			}
			this.colorEdges()

			// Vertex/edge labels
			if (!this.labelGroup)
				this.labelGroup = this.svg.append("g")
			if (!this.vertexLabelGroup)
				this.vertexLabelGroup = this.labelGroup.append("g")
			this.vertexLabels = this.vertexLabelGroup.selectAll("text")
				.data(this.vertices)
				.join("text")
				.text(d => this.ellipseLabelTextIfNeeded(d.labels.join(", ")))
				.attr("text-anchor", "middle")
				.attr("cursor", "default")
			if (!this.edgeLabelGroup)
				this.edgeLabelGroup = this.labelGroup.append("g")
			this.edgeLabels = this.edgeLabelGroup.selectAll("text")
				.data(this.edges)
				.join("text")
				.text(d => this.ellipseLabelTextIfNeeded(d.label))
				.attr("text-anchor", "middle")
				.attr("cursor", "default")

			// Adding hover behaviour
			this.vertexCircles
				.on("mouseover", this.vertexHoverEnter)
				.on("mouseout", this.vertexOrEdgeHoverExit)
			this.vertexNumbers
				.on("mouseover", this.vertexHoverEnter)
				.on("mouseout", this.vertexOrEdgeHoverExit)
			this.edgeLines
				.on("mouseover", this.edgeHoverEnter)
				.on("mouseout", this.vertexOrEdgeHoverExit)
			this.edgeLabels
				.on("mouseover", this.edgeHoverEnter)
				.on("mouseout", this.vertexOrEdgeHoverExit)

			this.svg.call(this.zoomBehaviour)

			this.forceSimulation.on("tick", this.plotChangingAttributes)

			const dragBehaviour = D3Utils.forceSimulationDragBehaviour(this.forceSimulation)
			this.vertexCircles.call(dragBehaviour)
			this.vertexNumbers.call(dragBehaviour)
		},
		plotChangingAttributes: function() {
			const zoomTransform = d3.zoomTransform(this.svg.node())
			this.vertexCircles
				.attr("r", zoomTransform.k * VERTEX_CIRCLE_BASE_RADIUS)
				.attr("stroke-width", zoomTransform.k * VERTEX_CIRCLE_BASE_STROKE_WIDTH)
				.attr("cx", d => zoomTransform.applyX(d.x))
				.attr("cy", d => zoomTransform.applyY(d.y))
			this.vertexNumbers
				.attr("visibility", this.showVertexNumbers ? "visible" : "hidden")
				.attr("font-size", zoomTransform.k * VERTEX_NUMBER_BASE_FONT_SIZE)
				.attr("x", d => zoomTransform.applyX(d.x))
				.attr("y", d => zoomTransform.applyY(d.y) + zoomTransform.k * (VERTEX_NUMBER_BASE_FONT_SIZE / 2))
			this.edgeLines
				.attr("d", d => D3Utils.edgePath(d, zoomTransform, EDGE_LINES_BASE_SPREAD))
				.attr("stroke-width", zoomTransform.k * EDGE_LINE_BASE_STROKE_WIDTH)
			if (this.edgeArrows !== null) {
				this.edgeArrows
					.attr("d", d => D3Utils.edgeArrowHeadPath(d, zoomTransform, EDGE_LINES_BASE_SPREAD, VERTEX_CIRCLE_BASE_RADIUS, EDGE_ARROW_BASE_SIZE))
					.attr("stroke-width", zoomTransform.k * EDGE_LINE_BASE_STROKE_WIDTH * 0.75) // We make the arrow slightly thinner than the edges
			}
			this.vertexLabels
				.attr("visibility", this.showLabels ? "visible" : "hidden")
				.attr("x", d => zoomTransform.applyX(d.x))
				.attr("y", d => zoomTransform.applyY(d.y) - zoomTransform.k * VERTEX_CIRCLE_BASE_RADIUS)
				.attr("font-size", Math.max(zoomTransform.k * LABELS_BASE_FONT_SIZE.normal, LABELS_BASE_FONT_SIZE.min))
			this.edgeLabels
				.attr("visibility", this.showLabels ? "visible" : "hidden")
				.attr("x", d => D3Utils.edgeLabelPosition(d, zoomTransform, EDGE_LINES_BASE_SPREAD).x)
				.attr("y", d => D3Utils.edgeLabelPosition(d, zoomTransform, EDGE_LINES_BASE_SPREAD).y)
				.attr("font-size", Math.max(zoomTransform.k * LABELS_BASE_FONT_SIZE.normal, LABELS_BASE_FONT_SIZE.min))
		},
		resetZoom: function () {
			this.svg
				.transition()
				.duration(750)
				.call(this.zoomBehaviour.transform, d3.zoomIdentity)
		},
		vertexHoverEnter: function(_event, d) {
			let text = "v" + d.vertexNumber
			if (d.isPort)
				text += ", Port"
			if (d.labels.length === 0)
				text += ", No labels"
			else if (d.labels.length === 1)
				text += ", Label: "
			else
				text += ", Labels: "
			text += d.labels.join(", ")
			this.vertexOrEdgeHoverEnter(text)

		},
		edgeHoverEnter: function(_event, d) {
			this.vertexOrEdgeHoverEnter("Label: " + d.label)
		},
		vertexOrEdgeHoverEnter: function (text) {
			if (!this.hoverTextGroup)
				this.hoverTextGroup = this.svg.append("g")

			this.hoverTextGroup.selectAll("text").remove()
			const hoverText = this.hoverTextGroup.append("text") // Creating the top text
				.text(text)
				.attr("x", 10)
				.attr("y", 30)
			hoverText.clone(true) // Creating bottom text
				.attr("y", this.height - 30)
		},
		vertexOrEdgeHoverExit: function () {
			if (this.hoverTextGroup)
				this.hoverTextGroup.selectAll("text").remove()
		},
		colorVertices: function () { // Change the color of all vertices
			this.vertexCircles.attr("stroke", d => this.vertexColorFromLabels(d.labels))
			this.vertexCircles.attr("fill", d => (d.isPort && !this.showPortsAsFullCircles) ? "white" : this.vertexColorFromLabels(d.labels))
		},
		colorEdges: function () { // Change the color of all edges
			this.edgeLines.attr("stroke", d => this.edgeColorFromLabel(d.label))
			if (this.edgeArrows !== null){
				this.edgeArrows.attr("stroke", d => this.edgeColorFromLabel(d.label))
			}
		},
		ellipseLabelTextIfNeeded: function (labelText) {
			if (labelText.length <= LABELS_MAX_CHARS)
				return labelText
			else
				return labelText.slice(0, LABELS_MAX_CHARS - 3) + "..."
		},
	},
	watch: {
		pattern: function () {
			this.plot()
		},
		vertexRepulsionStrength: function () {
			if (this.forceSimulation != null) {
				this.forceSimulation.force("vertexRepulsive", this.vertexRepulsiveForce)
				D3Utils.reIgniteSimulation(this.forceSimulation)
			}
		},
		showVertexNumbers: function () {
			if(this.vertexNumbers) // The text elements have already been created
				this.plotChangingAttributes()
		},
		showPortsAsFullCircles: function () {
			if (this.vertexCircles) // The vertices circles have already been created
				this.colorVertices()
		},
		showLabels: function () {
			if (this.labelGroup)
				this.plotChangingAttributes()
		},
	},
	mounted() {
		this.plot()
	},
}
</script>
<style scoped lang="scss">
@import "bootstrap/scss/bootstrap";

.graph-controls {
	> div {
		margin-bottom: 1em;

		&.repulsion-control {
			label {
				@extend .form-label;
			}

			input {
				@extend .form-control;
				width: auto;
				display: inline-block;
				margin-left: 1em;
			}
		}

		&.checkboxes {
			.checkbox-control {
				@extend .form-check;
				@extend .form-check-inline;

				label {
					@extend .form-check-label;
				}

				input {
					@extend .form-check-input;
				}
			}
		}

		&.zoom-controls {
			@extend .col-auto;

			button {
				@extend .btn;
				@extend .btn-outline-secondary;
			}
		}
	}
}
</style>
