export default {
	graphMDL: [
		{
			datasetName: "AIDS-CA",
			datasetURL: "https://wiki.nci.nih.gov/display/NCIDTPdata/AIDS+Antiviral+Screen+Data",
			subsets: [
				{
					desc: "Data subsets for AIDS-CA molecules",
					shortName: "AIDS-CA data subsets",
					path: "/ExampleFiles/aids_CA-data_subsets.json.zip",
				},
			],
			experiments: [
				{
					desc: "GraphMDL result file for 2190 candidate patterns extracted with 20% support",
					shortName: "GraphMDL on AIDS-CA 20% support",
					path: "/ExampleFiles/AIDS-CA-graphmdl-0.2support.json.zip",
				},
				{
					desc: "GraphMDL result file for 7862 candidate patterns extracted with 15% support",
					shortName: "GraphMDL on AIDS-CA 15% support",
					path: "/ExampleFiles/AIDS-CA-graphmdl-0.15support.json.zip",
				},
				{
					desc: "GraphMDL result file for 21k candidate patterns extracted with 10% support",
					shortName: "GraphMDL on AIDS-CA 10% support",
					path: "/ExampleFiles/AIDS-CA-graphmdl-0.1support.json.zip",
				},
			],
		},
		{
			datasetName: "AIDS-CM",
			datasetURL: "https://wiki.nci.nih.gov/display/NCIDTPdata/AIDS+Antiviral+Screen+Data",
			subsets: [
				{
					desc: "Data subsets for AIDS-CM molecules",
					shortName: "AIDS-CM data subsets",
					path: "/ExampleFiles/AIDS-CM-data_subsets.json.zip",
				},
			],
			experiments: [
				{
					desc: "GraphMDL result file for 429 candidate patterns extracted with 20% support",
					shortName: "GraphMDL on AIDS-CM 20% support",
					path: "/ExampleFiles/AIDS-CM-graphmdl-0.2support.json.zip",
				},
				{
					desc: "GraphMDL result file for 774 candidate patterns extracted with 15% support",
					shortName: "GraphMDL on AIDS-CM 15% support",
					path: "/ExampleFiles/AIDS-CM-graphmdl-0.15support.json.zip",
				},
				{
					desc: "GraphMDL result file for 2049 candidate patterns extracted with 10% support",
					shortName: "GraphMDL on AIDS-CM 10% support",
					path: "/ExampleFiles/AIDS-CM-graphmdl-0.1support.json.zip",
				},
				{
					desc: "GraphMDL result file for 9937 candidate patterns extracted with 5% support",
					shortName: "GraphMDL on AIDS-CM 5% support",
					path: "/ExampleFiles/AIDS-CM-graphmdl-0.05support.json.zip",
				},
			],
		},
		{
			datasetName: "Mutag",
			datasetURL: "http://networkrepository.com/Mutag.php",
			subsets: [
				{
					desc: "Data subsets for Mutag Molecules",
					shortName: "Mutag data subsets",
					path: "/ExampleFiles/Mutag-data_subsets.json.zip",
				},
			],
			experiments: [
				{
					desc: "GraphMDL result file for 656 candidate patterns extracted with 40% support",
					shortName: "GraphMDL on Mutag 40% support",
					path: "/ExampleFiles/Mutag-positive-graphmdl-0.40support.json.zip",
				},
				{
					desc: "GraphMDL result file for 1766 candidate patterns extracted with 35% support",
					shortName: "GraphMDL on Mutag 35% support",
					path: "/ExampleFiles/Mutag-positive-graphmdl-0.35support.json.zip",
				},
				{
					desc: "GraphMDL result file for 5395 candidate patterns extracted with 30% support",
					shortName: "GraphMDL on Mutag 30% support",
					path: "/ExampleFiles/Mutag-positive-graphmdl-0.30support.json.zip",
				},
				{
					desc: "GraphMDL result file for 15k candidate patterns extracted with 25% support",
					shortName: "GraphMDL on Mutag 25% support",
					path: "/ExampleFiles/Mutag-positive-graphmdl-0.25support.json.zip",
				},
			],
		},
		{
			datasetName: "PTC",
			datasetURL: "http://networkrepository.com/PTC-FM.php",
			subsets: [],
			experiments: [
				{
					desc: "GraphMDL result file for 80k candidate patterns extracted from PTC-FM with 1% support",
					shortName: "GraphMDL on PTC-FM 1% support",
					path: "/ExampleFiles/PTC_FM-positive-graphmdl-0.01support.json.zip",
				},
				{
					desc: "GraphMDL result file for 20k candidate patterns extracted from PTC-FR with 3% support",
					shortName: "GraphMDL on PTC-FR 3% support",
					path: "/ExampleFiles/PTC_FR-positive-graphmdl-0.03support.json.zip",
				},
				{
					desc: "GraphMDL result file for 78k candidate patterns extracted from PTC-MM with 1% support",
					shortName: "GraphMDL on PTC-MM 1% support",
					path: "/ExampleFiles/PTC_MM-positive-graphmdl-0.01support.json.zip",
				},
				{
					desc: "GraphMDL result file for 16kk candidate patterns extracted from PTC-MR with 3% support",
					shortName: "GraphMDL on PTC-MR 3% support",
					path: "/ExampleFiles/PTC_MR-positive-graphmdl-0.03support.json.zip",
				},
			],
		},
		{
			datasetName: "Universal Dependencies PUD-EN dataset",
			datasetURL: "https://universaldependencies.org/",
			subsets: [
				{
					desc: "Data subsets for UD-PUD-En sentences",
					shortName: "UD-PUD-En data subsets",
					path: "/ExampleFiles/UD-PUD-En-no_punct-data_subsets.json.zip",
				},
			],
			experiments: [
				{
					desc: "GraphMDL result file for 6006 candidate patterns extracted with 1% support",
					shortName: "GraphMDL on UD-PUD-En 1% support",
					path: "/ExampleFiles/UD-PUD-En-no_punct-graphmdl-0.01support.json.zip",
				},
				{
					desc: "GraphMDL result file for 21k candidate patterns extracted with 0.5% support",
					shortName: "GraphMDL on UD-PUD-En 0.05% support",
					path: "/ExampleFiles/UD-PUD-En-no_punct-graphmdl-0.005support.json.zip",
				},
				{
					desc: "GraphMDL result file for 233k candidate patterns extracted with 0% support",
					shortName: "GraphMDL on UD-PUD-En 0% support",
					path: "/ExampleFiles/UD-PUD-En-no_punct-graphmdl-0support.json.zip",
				},
			],
		},
	],
	graphMDLPlus: [
		{
			desc: "GraphMDL+ on AIDS-CA. Approach ran in 1h17m",
			path: "/ExampleFiles/aids_CA-graphmdlplus.json.zip",
			shortName: "GraphMDL+ on AIDS-CA",
		},
		{
			desc: "GraphMDL+ on AIDS-CM. Approach stopped after 4h",
			path: "/ExampleFiles/aids_CM-graphmdlplus.json.zip",
			shortName: "GraphMDL+ on AIDS-CM",
		},
		{
			desc: "GraphMDL+ on Mutag. Approach ran in 1m",
			path: "/ExampleFiles/Mutag-positive-graphmdlplus.json.zip",
			shortName: "GraphMDL+ on Mutag",
		},
		{
			desc: "GraphMDL+ on PTC-FM. Approach ran in 1m",
			path: "/ExampleFiles/PTC_FM-positive-graphmdlplus.json.zip",
			shortName: "GraphMDL+ on PTC-FM",
		},
		{
			desc: "GraphMDL+ on PTC-FR. Approach ran in 1m",
			path: "/ExampleFiles/PTC_FR-positive-graphmdlplus.json.zip",
			shortName: "GraphMDL+ on PTC-FR",
		},
		{
			desc: "GraphMDL+ on PTC-MM. Approach ran in 1m",
			path: "/ExampleFiles/PTC_MM-positive-graphmdlplus.json.zip",
			shortName: "GraphMDL+ on PTC-MM",
		},
		{
			desc: "GraphMDL+ on PTC-MR. Approach ran in 1m",
			path: "/ExampleFiles/PTC_MR-positive-graphmdlplus.json.zip",
			shortName: "GraphMDL+ on PTC-MR",
		},
		{
			desc: "GraphMDL+ on UD-PUD-En (no punctuation, undirected version). Approach ran in 2h32m",
			path: "/ExampleFiles/UD-PUD-En-no_punct-graphmdlplus-undirected.json.zip",
			shortName: "GraphMDL+ on UD-PUD-En no punct, undirected",
		},
		{
			desc: "GraphMDL+ on UD-PUD-En (no punctuation, directed version). Approach ran in 1h03m",
			path: "/ExampleFiles/UD-PUD-En-no_punct-graphmdlplus-directed.json.zip",
			shortName: "GraphMDL+ on UD-PUD-En no punct, directed",
		},
	],
	kgMDL: [
		{
			datasetName: "NTNames",
			datasetURL: "http://www.semanticbible.com/ntn/ntn-overview.html",
			experiments: [
				{
					desc: "KG-MDL on NTNames, 200ms cover timeout",
					shortName: "KG-MDL on NTNames, 200ms cover timeout",
					path: "/ExampleFiles/KGMDL-NTNames-rto200.json.zip",
				},
			],
		},
		{
			datasetName: "Lemon",
			datasetURL: "https://lemon-model.net/lexica/dbpedia_en/",
			experiments: [
				{
					desc: "KG-MDL on Lemon, 200ms cover timeout",
					shortName: "KG-MDL on Lemon, 200ms cover timeout",
					path: "/ExampleFiles/KGMDL-Lemon-rto200.json.zip",
				},
			],
		},
		{
			datasetName: "Mondial",
			datasetURL: "https://www.dbis.informatik.uni-goettingen.de/Mondial/",
			experiments: [
				{
					desc: "KG-MDL on Mondial (europe subset), 500ms cover timeout",
					shortName: "KG-MDL on Mondial-europe, 500ms cover timeout",
					path: "/ExampleFiles/KGMDL-Mondial-europe-rto500.json.zip",
				},
				{
					desc: "KG-MDL on Mondial (whole data), 500ms cover timeout",
					shortName: "KG-MDL on Mondial, 500ms cover timeout",
					path: "/ExampleFiles/KGMDL-Mondial-rto500.json.zip",
				},
			],
		},
		{
			datasetName: "Taaable",
			// datasetURL: "", // Not available online
			experiments: [
				{
					desc: "KG-MDL on Taaable, 500ms cover timeout",
					shortName: "KG-MDL on Taaable, 500ms cover timeout",
					path: "/ExampleFiles/KGMDL-Taaable-rto500.json.zip",
				},
			],
		},
	],
}
