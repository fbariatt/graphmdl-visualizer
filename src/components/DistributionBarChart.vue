<template>
<div>
	<svg ref="svg" :width="width" :height="height"></svg>
	<div class="median-help-text">The vertical dotted line represents the median</div>
</div>
</template>

<script>
import * as d3 from "d3"

export default {
	name: "DistributionBarChart",
	props: {
		data: {
			type: Array,
			required: true,
		},
		/**
		 * Minimum value of x that should always appear in the graph, even if there are no elements with that value in the data.
		 * E.g. when plotting the edge count distribution, we may always want a "0 edge" column even if there are no patterns with 0 edges, to show exactly that.
		 */
		xMin: {
			type: Number,
			default: 1,
		},
		width: {
			type: Number,
			default: 500,
		},
		height: {
			type: Number,
			default: 300,
		},
		margin: {
			type: Object,
			default: () => ({
				top: 20,
				bottom: 20,
				left: 40,
				right: 20,
			}),

		},
		barPadding: {
			type: Number,
			default: 0.2,
		},
		barColor: {
			type: String,
			default: "steelblue",
		},
		medianLineColor: {
			type: String,
			default: "red",
		},
		medianLineStrokeWidth: {
			type: Number,
			default: 2,
		},
	},
	data() {
		return {
			xScale: null,
			yScale: null,
			xAxis: null,
			yAxis: null,
			bars: null,
			medianLine: null,
			selectedBarValueText: null,
		}
	},
	computed: {
		svg: function () {
			return d3.select(this.$refs.svg)
		},
		bins: function () {
			const countsByValue = this.data.reduce(
				(accum, value) => {
					const currentCount = accum.get(value) ?? {value, nbElements: 0}
					currentCount.nbElements++
					return accum.set(value, currentCount)
				},
				new Map(),
			)
			return Array.from(countsByValue.values())
				.sort((countA, countB) => countA.value - countB.value) // The fact that the bins are sorted makes animation easier (delay can be based on their index)
		},
	},
	methods: {
		plot: function () {
			// Creating the scales
			// Remember that in svg (0,0) is in the top left corner
			this.xScale= d3.scaleBand()
				.domain(d3.range(this.xMin, d3.max(this.bins, bin => bin.value) + 1)) // All values between xMin and the max x value in the data (range does not include end, hence the +1)
				.range([this.margin.left, this.width - this.margin.right])
				.padding(this.barPadding)
			this.yScale = d3.scaleLinear()
				.domain([0, d3.max(this.bins, bin => bin.nbElements)])
				.range([this.height - this.margin.bottom, this.margin.top])
				.nice()

			// Transition used for all animations (scale and bars)
			const transition = d3.transition()
				.duration(750)

			// Creating the axis
			if (!this.xAxis)
				this.xAxis = this.svg.append("g")
			this.xAxis
				.attr("transform", "translate(0,"+(this.height - this.margin.bottom)+")")
				.transition(transition)
				.call(d3.axisBottom(this.xScale))
			if (!this.yAxis)
				this.yAxis = this.svg.append("g")
			this.yAxis
				.attr("transform", "translate("+this.margin.left+",0)")
				.transition(transition)
				.call(d3.axisLeft(this.yScale))
			
			// Creating the text element for when a bar is hovered
			if (!this.selectedBarValueText)
				this.selectedBarValueText = this.svg.append("text")
			this.selectedBarValueText
				.attr("fill", "#2A2A2A")
				.attr("font-style", "italic")
				.attr("x", this.margin.left - 7)
				.attr("y", this.margin.top - 7)
				.text("")

			// Creating the bars
			if (!this.bars)
				this.bars = this.svg.append("g")
			this.bars.selectAll("rect")
				.data(this.bins)
				.join("rect")
				.attr("fill", this.barColor)
				.attr("x", d => this.xScale(d.value))
				.on("mouseover", (e, d) => {
					d3.select(e.currentTarget)
						.attr("fill", d3.color(this.barColor).brighter())
					this.selectedBarValueText
						.text(d.nbElements)
				})
				.on("mouseout", e => {
					d3.select(e.currentTarget)
						.attr("fill", this.barColor)
					this.selectedBarValueText
						.text("")
				})
				.attr("y", this.yScale(0))
				.attr("height", 0)
				.transition(transition).delay((_d, i) => i * 20)
				// We need attrTween, since y must grow backwards (be big at begin of transition and small at the end), since (0,0) is top-left
				.attrTween("y", bin => d3.interpolateNumber(this.yScale(0), this.yScale(bin.nbElements)))
				.attrTween("height", bin => d3.interpolateNumber(0, this.yScale(0) - this.yScale(bin.nbElements)))

			// Median line
			if (!this.medianLine)
				this.medianLine = this.svg.append("g")
			this.medianLine
				.selectAll("line")
				.data([Math.round(d3.median(this.data))])
				.join("line")
				.attr("stroke", this.medianLineColor)
				.attr("stroke-dasharray", 2)
				.attr("stroke-width", this.medianLineStrokeWidth)
				.attr("x1", d => this.xScale(d) + this.xScale.bandwidth() / 2)
				.attr("x2", d => this.xScale(d) + this.xScale.bandwidth() / 2)
				.attr("y1", this.height - this.margin.bottom)
				.attr("y2", this.margin.top)
				.attr("stroke-opacity", 0)
				.transition(transition)
				.attr("stroke-opacity", 1)

			// Adding the possibility of zooming on the graph
			const zoomBehaviour = d3.zoom()
				.scaleExtent([1, 20]) // Setting minimum zoom (1x, meaning one can not zoom out), and maximum zoom (20x)
				.on("zoom", event => {
					const newXScale = this.xScale.copy()
						.range(this.xScale.range().map(d => event.transform.applyX(d)))
					this.xAxis
						.call(d3.axisBottom(newXScale))
					this.bars.selectAll("rect")
						.attr("x", d => newXScale(d.value))
						.attr("width", newXScale.bandwidth())
					this.medianLine.selectAll("line")
						.attr("x1", d => newXScale(d) + newXScale.bandwidth() / 2)
						.attr("x2", d => newXScale(d) + newXScale.bandwidth() / 2)
				})
			this.svg.call(zoomBehaviour)
			// In case there was a previously applied zoom (e.g. from previous data), we reset it
			zoomBehaviour.transform(this.svg, d3.zoomIdentity)
		},
	},
	watch: {
		data() {
			this.plot()
		},
	},
	mounted() {
		this.plot()
	},
}
</script>

<style scoped lang="scss">

.median-help-text {
	font-style: italic;
	font-weight: lighter;
}
</style>
