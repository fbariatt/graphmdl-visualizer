<template>
<div>
	<div class="repulsion-control">
		<label for="RewrittenGraph.vertexRepulsionStrength">Vertex repulsion strength</label>
		<input type="number" v-model="vertexRepulsionStrength" id="RewrittenGraph.vertexRepulsionStrength"/>
	</div>
	<div class="checkboxes">
		<div>
			<label for="RewrittenGraph.showVertexNumbers">Show vertex numbers</label>
			<input type="checkbox" v-model="showVertexNumbers" id="RewrittenGraph.showVertexNumbers"/>
		</div>
		<div>
			<label for="RewrittenGraph.showLabels">Show labels</label>
			<input type="checkbox" v-model="showLabels" id="RewrittenGraph.showLabels">
		</div>
	</div>
	<div class="buttons">
		<button @click="onToggleSimulationStatus" class="simulation-toggle" :class="{start: !simulationStarted, stop: simulationStarted}">
			{{ simulationStarted ? "Stop" : "Start" }} simulation
		</button>
		<button v-if="!askConfirmStart" @click="resetZoom" class="zoom-reset">Reset zoom</button>
	</div>
	<div class="ask-confirm-start" v-if="askConfirmStart">
		Warning: this rewritten graph contains {{ filteredEmbeddings.length + filteredPorts.length }} vertices, which may be too many to plot.
		Press "start simulation" to plot anyway.
	</div>
	<svg ref="svg" v-show="!askConfirmStart" :width="width" :height="height"></svg>
	<div class="ports-help-text">
		The small white circles represent port vertices of the rewritten graph. Each one corresponds to a vertex in the	input data.
		Pattern vertices connected to ports via dotted lines correspond to the same vertex in the data.
		See the <a href="https://link.springer.com/chapter/10.1007%2F978-3-030-44584-3_5" target="_blank">GraphMDL paper</a> for more information.
	</div>
</div>
</template>

<script>
import * as d3 from "d3"
import D3Utils from "../D3Utils"
import {GraphType} from "../GraphType"
import PatternUtils from "../PatternUtils"

const VertexType = Object.freeze({PATTERN_VERTEX: "e", PORT: "p"})
const EdgeType = Object.freeze({PORT_EDGE: "portE", PATTERN_EDGE: "patternE"})

const PATTERN_VERTEX_CIRCLE_BASE_RADIUS = Object.freeze({hovered: 7, normal: 5})
const PATTERN_EDGE_BASE_STROKE_WIDTH = Object.freeze({hovered: 3, normal: 2})
const PATTERN_EDGE_ARROW_BASE_SIZE = 7
const PATTERN_EDGE_LINES_BASE_SPREAD = 40
const PORT_VERTEX_CIRCLE_BASE_RADIUS = 5
const PORT_EDGE_BASE_STROKE_WIDTH = 2
const VERTEX_NUMBER_BASE_FONT_SIZE = Object.freeze({hovered: 7, normal: 5})
const EDGE_LABEL_BASE_FONT_SIZE = Object.freeze({normal: 7, min: 10})
const VERTEX_LABEL_BASE_FONT_SIZE = Object.freeze({normal: 7, min: 10})
const LABELS_MAX_CHARS = 30

export default {
	name: "RewrittenGraphPlot",
	props: {
		patterns: {
			type: Array,
			required: true,
		},
		graphType: {
			default: GraphType.SIMPLE_UNDIRECTED,
		},
		dataVertexFilter: { // Only print embeddings and ports which use at least one data vertex allowed by this filter
			type: Function,
			required: true,
		},
		vertexColorFromLabels: { // Called to get the color of a vertex. Should expect an array of vertex labels.
			type: Function,
			required: true,
		},
		edgeColorFromLabel: { // Called to get the color of an edge. Should expect a string.
			type: Function,
			required: true,
		},
		width: {
			type: Number,
			default: 500,
		},
		height: {
			type: Number,
			default: 500,
		},
	},
	data() {
		return {
			askConfirmStart: false, // Whether we should ask for user confirmation before running the simulation
			embeddingsToSimulationNodes: new Map(), // Embedding number in filteredEmbeddings -> array of simulation nodes for each of the pattern's vertices
			portsToSimulationNodes: new Map(), // Data vertex number -> simulation node
			simulationNodes: [], // The two simulation nodes together: pattern vertices and ports
			simulationLinks: [],
			forceSimulation: null,
			vertexRepulsionStrength: 150,
			circleGroup: null,
			circles: null,
			portEdgeLinesGroup: null,
			portEdgeLines: null,
			patternEdgeLinesGroup: null,
			patternEdgeLines: null,
			patternEdgeArrowsGroup: null,
			patternEdgeArrows: null,
			hoverTextGroup: null,
			hoveredEmbedding: null,
			simulationStarted: true,
			showVertexNumbers: false,
			vertexNumberGroup: null,
			vertexNumbers: null,
			showLabels: false,
			labelGroup: null,
			vertexLabelGroup: null,
			vertexLabels: null,
			patternEdgeLabelsGroup: null,
			patternEdgeLabels: null,
		}
	},
	computed: {
		svg: function() {
			return d3.select(this.$refs.svg)
		},
		/**
		 * All used embeddings of all patterns with at least one data vertex that passes the data vertex filter.
		 * On top of its normal structure, each returned embedding will have a "pattern" field that indicates the number of the pattern it is from.
		 */
		filteredEmbeddings: function() {
			return this.patterns.flatMap((pattern, patternIndex) =>
				pattern["used_embeddings"]
					.filter(embedding => embedding.mapping.some(dataVertex => this.dataVertexFilter(dataVertex)))
					.map(embedding => Object.assign({}, embedding, {pattern: patternIndex})),
			)
		},
		/**
		 * A list of all the data vertices used as ports by embeddings that are accepted by the filter
		 */
		filteredPorts: function() {
			return Array.from(new Set(this.filteredEmbeddings.flatMap(embedding => Object.values(embedding.ports))))
		},
		zoomBehaviour: function() {
			return d3.zoom()
				.scaleExtent([0.2, 20])
				.on("zoom", this.plotChangingAttributes)
		},
		vertexRepulsiveForce: function() {
			return d3.forceManyBody().strength(-this.vertexRepulsionStrength)
		},
		simulationPortEdges: function() {
			return this.simulationLinks.filter(d => d.type === EdgeType.PORT_EDGE)
		},
		simulationPatternEdges: function() {
			return this.simulationLinks.filter(d => d.type === EdgeType.PATTERN_EDGE)
		},
	},
	methods: {
		createSimulationNodes: function() {
			let simulationNodesCount = 0
			this.simulationNodes = []

			// Simulation nodes for each of the pattern vertices of the embeddings
			this.embeddingsToSimulationNodes = new Map()
			for (let embeddingIndex = 0; embeddingIndex < this.filteredEmbeddings.length; embeddingIndex++) {
				const embedding = this.filteredEmbeddings[embeddingIndex]
				let embeddingSimulationNodes = []
				for (let v = 0; v < embedding.mapping.length; ++v) {
					const simulationNode = {
						index: simulationNodesCount,
						type: VertexType.PATTERN_VERTEX,
						labels: this.patterns[embedding.pattern].structure.vertices[v],
						embedding: embeddingIndex,
						patternVertex: v,
					}
					embeddingSimulationNodes.push(simulationNode)
					this.simulationNodes.push(simulationNode)
					simulationNodesCount++
				}
				this.embeddingsToSimulationNodes.set(embeddingIndex, embeddingSimulationNodes)
			}

			// Simulation nodes for each of the port vertices
			this.portsToSimulationNodes = new Map()
			for (let dataVertex of this.filteredPorts) {
				const simulationNode = {
					index: simulationNodesCount,
					type: VertexType.PORT,
					dataVertex: dataVertex,
				}
				this.portsToSimulationNodes.set(dataVertex, simulationNode)
				this.simulationNodes.push(simulationNode)
				simulationNodesCount++
			}
		},
		createSimulationLinks: function() {
			this.simulationLinks = []

			// Pattern edges
			this.filteredEmbeddings.forEach((embedding, embeddingIndex) => {
				const pattern = this.patterns[embedding.pattern] // Getting the pattern associated to the embedding
				const embeddingSimulationNodes = this.embeddingsToSimulationNodes.get(embeddingIndex) // Getting the simulation nodes that represent each of this embedding's vertices
				pattern.structure.edges.forEach(edge => { // For all edges of the pattern
					// We create a corresponding link in the simulation
					const simulationLink = {
						type: EdgeType.PATTERN_EDGE,
						source: embeddingSimulationNodes[edge.source].index,
						target: embeddingSimulationNodes[edge.target].index,
						label: edge.label,
						embedding: embeddingIndex,
					}
					this.simulationLinks.push(simulationLink)
				})
			})
			const simulationLinksGroupedByEnd = PatternUtils.groupEdgesByEnds(this.simulationLinks)
			simulationLinksGroupedByEnd.forEach(linksWithSameEnds => {
				const count = linksWithSameEnds.length
				linksWithSameEnds.forEach((link, i) => {
					link.totalWithSameEnds = count // Each edge knows how many vertices are between the same pair of vertices
					link.indexOfSameEnds = i // Index of this edge between all edges that have the same ends
				})
			})

			// Port of pattern to port vertices
			this.filteredEmbeddings.forEach((embedding, embeddingIndex) => {
				const verticesOfEmbedding = this.embeddingsToSimulationNodes.get(embeddingIndex)
				for (let port of Object.keys(embedding.ports)) {
					const simulationLink = {
						type: EdgeType.PORT_EDGE,
						source: verticesOfEmbedding[port].index,
						target: this.portsToSimulationNodes.get(embedding.ports[port]).index,
					}
					this.simulationLinks.push(simulationLink)
				}
			})

		},
		createSimulation: function() {
			this.createSimulationNodes()
			this.createSimulationLinks()

			if (this.forceSimulation) // If there is a running simulation already (which normally should not because of where we call this function), we make sure to stop it
				this.forceSimulation.stop()

			this.forceSimulation = d3.forceSimulation(this.simulationNodes)
				.force("vertexRepulsive", this.vertexRepulsiveForce)
				.force("link", d3.forceLink(this.simulationLinks).distance(d => d.type === EdgeType.PATTERN_EDGE ? 40 : 15))
				.force("x", d3.forceX(this.width / 2))
				.force("y", d3.forceY(this.height / 2))
			this.simulationStarted = true // Simulations start when they are created
		},
		/**
		 * Plot the rewritten graph, but only if it is not too big. Otherwise, ask for user confirmation before plotting.
		 * This avoids an unannounced freeze in case the user selects a data subset with too many vertices as once.
		 */
		plotIfNotTooBig: function() {
			// If there is a running simulation already (maybe we called this method after changing the data), we make sure to stop it
			if (this.forceSimulation)
				this.forceSimulation.stop()

			if (this.filteredEmbeddings.length + this.filteredPorts.length < 150) // There aren't too many nodes in the simulation
			{
				this.askConfirmStart = false
				this.plot()
			} else {
				this.forceSimulation = null
				this.simulationStarted = false
				this.askConfirmStart = true
			}
		},
		plot: function() {
			this.createSimulation()

			// We add edge elements (lines, arrows) before vertex elements, so that they are under them

			// Pattern edge lines
			if (!this.patternEdgeLinesGroup)
				this.patternEdgeLinesGroup = this.svg.append("g")
			this.patternEdgeLines = this.patternEdgeLinesGroup.selectAll("path")
				.data(this.simulationPatternEdges)
				.join("path")
				.attr("fill-opacity", 0)

			// Edge arrow heads if graph is directed
			if (!this.patternEdgeArrowsGroup)
				this.patternEdgeArrowsGroup = this.svg.append("g")
			if (GraphType.isDirected(this.graphType)) {
				this.patternEdgeArrows = this.patternEdgeArrowsGroup.selectAll("path")
					.data(this.simulationPatternEdges)
					.join("path")
					.attr("fill-opacity", 0)
			} else {
				if (this.patternEdgeArrows !== null)
					this.patternEdgeArrows.remove()
				this.patternEdgeArrows = null
			}

			this.colorEdges()

			// Port edge lines
			if (!this.portEdgeLinesGroup)
				this.portEdgeLinesGroup = this.svg.append("g")
			this.portEdgeLines = this.portEdgeLinesGroup.selectAll("line")
				.data(this.simulationPortEdges)
				.join("line")
				.attr("stroke-dasharray", 4)
				.attr("stroke", "#555")


			// Pattern vertices and ports
			if (!this.circleGroup)
				this.circleGroup = this.svg.append("g")
			this.circles = this.circleGroup.selectAll("circle")
				.data(this.simulationNodes)
				.join("circle")
			this.colorVertices()

			if (!this.vertexNumberGroup)
				this.vertexNumberGroup = this.svg.append("g")
			this.vertexNumbers = this.vertexNumberGroup.selectAll("text")
				.data(this.simulationNodes.filter(d => d.type === VertexType.PATTERN_VERTEX))
				.join("text")
				.text(d => d.patternVertex)
				.attr("text-anchor", "middle")
				.attr("cursor", "default")

			// Vertex/Edge labels
			if (!this.labelGroup)
				this.labelGroup = this.svg.append("g")
			if (!this.vertexLabelGroup)
				this.vertexLabelGroup = this.labelGroup.append("g")
			this.vertexLabels = this.vertexLabelGroup.selectAll("text")
				.data(this.simulationNodes.filter(d => d.type === VertexType.PATTERN_VERTEX))
				.join("text")
				.text(d => this.ellipseLabelTextIfNeeded(d.labels.join(", ")))
				.attr("text-anchor", "middle")
				.attr("cursor", "default")
			if (!this.patternEdgeLabelsGroup)
				this.patternEdgeLabelsGroup = this.labelGroup.append("g")
			this.patternEdgeLabels = this.patternEdgeLabelsGroup.selectAll("text")
				.data(this.simulationPatternEdges)
				.join("text")
				.text(d => this.ellipseLabelTextIfNeeded(d.label))
				.attr("text-anchor", "middle")
				.attr("cursor", "default")

			this.forceSimulation.on("tick", this.plotChangingAttributes)

			const dragBehaviour = D3Utils.forceSimulationDragBehaviour(this.forceSimulation)
			this.circles.call(dragBehaviour)
			this.vertexNumbers.call(dragBehaviour)

			this.svg.call(this.zoomBehaviour)

			this.circles
				.on("mouseover", this.vertexHoverEnter)
				.on("mouseout", this.vertexHoverExit)
				.on("dblclick", this.onVertexDoubleClick)
			this.vertexNumbers
				.on("mouseover", this.vertexHoverEnter)
				.on("mouseout", this.vertexHoverExit)
				.on("dblclick", this.onVertexDoubleClick)
			this.patternEdgeLines
				.on("mouseover", this.edgeHoverEnter)
				.on("mouseout", this.edgeHoverExit)
			this.patternEdgeLabels
				.on("mouseover", this.edgeHoverEnter)
				.on("mouseout", this.edgeHoverExit)
		},
		plotChangingAttributes: function() {
			const zoomTransform = d3.zoomTransform(this.svg.node())
			this.circles
				.attr("cx", d => zoomTransform.applyX(d.x))
				.attr("cy", d => zoomTransform.applyY(d.y))
				.attr("r", d => {
					if (d.type === VertexType.PATTERN_VERTEX) {
						const baseRadius = d.embedding === this.hoveredEmbedding ? PATTERN_VERTEX_CIRCLE_BASE_RADIUS.hovered : PATTERN_VERTEX_CIRCLE_BASE_RADIUS.normal
						return zoomTransform.k * baseRadius
					} else {
						return PORT_VERTEX_CIRCLE_BASE_RADIUS // We decided that port vertices should not get bigger when zoomed
					}
				})
			this.vertexLabels
				.attr("visibility", this.showLabels ? "visible" : "hidden")
				.attr("x", d => zoomTransform.applyX(d.x))
				.attr("y", d => zoomTransform.applyY(d.y) - zoomTransform.k * PATTERN_VERTEX_CIRCLE_BASE_RADIUS.normal)
				.attr("font-size", Math.max(zoomTransform.k * VERTEX_LABEL_BASE_FONT_SIZE.normal, VERTEX_LABEL_BASE_FONT_SIZE.min)) // We enforce a minimum size for when the visualisation is zoomed out
			const vertexNumberFontSize = d => d.embedding === this.hoveredEmbedding ? VERTEX_NUMBER_BASE_FONT_SIZE.hovered : VERTEX_NUMBER_BASE_FONT_SIZE.normal
			this.vertexNumbers
				.attr("visibility", this.showVertexNumbers ? "visible" : "hidden")
				.attr("font-size", d => zoomTransform.k * vertexNumberFontSize(d))
				.attr("x", d => zoomTransform.applyX(d.x))
				.attr("y", d => zoomTransform.applyY(d.y) + zoomTransform.k * vertexNumberFontSize(d) / 2)

			this.patternEdgeLines
				.attr("d", d => D3Utils.edgePath(d, zoomTransform, PATTERN_EDGE_LINES_BASE_SPREAD))
				.attr("stroke-width", d => zoomTransform.k * (d.embedding === this.hoveredEmbedding ? PATTERN_EDGE_BASE_STROKE_WIDTH.hovered : PATTERN_EDGE_BASE_STROKE_WIDTH.normal))
			if (this.patternEdgeArrows !== null) {
				this.patternEdgeArrows
					.attr("d", d => D3Utils.edgeArrowHeadPath(
						d,
						zoomTransform,
						PATTERN_EDGE_LINES_BASE_SPREAD,
						d.embedding === this.hoveredEmbedding ? PATTERN_VERTEX_CIRCLE_BASE_RADIUS.hovered : PATTERN_VERTEX_CIRCLE_BASE_RADIUS.normal,
						PATTERN_EDGE_ARROW_BASE_SIZE,
					))
					.attr("stroke-width", d => zoomTransform.k * 0.75 * (d.embedding === this.hoveredEmbedding ? PATTERN_EDGE_BASE_STROKE_WIDTH.hovered : PATTERN_EDGE_BASE_STROKE_WIDTH.normal))
			}
			this.patternEdgeLabels
				.attr("visibility", this.showLabels ? "visible" : "hidden")
				.attr("x", d => D3Utils.edgeLabelPosition(d, zoomTransform, PATTERN_EDGE_LINES_BASE_SPREAD).x)
				.attr("y", d => D3Utils.edgeLabelPosition(d, zoomTransform, PATTERN_EDGE_LINES_BASE_SPREAD).y)
				.attr("font-size", Math.max(zoomTransform.k * EDGE_LABEL_BASE_FONT_SIZE.normal, EDGE_LABEL_BASE_FONT_SIZE.min)) // We enforce a minimum size for when the visualisation is zoomed out

			this.portEdgeLines
				.attr("x1", d => zoomTransform.applyX(d.source.x))
				.attr("y1", d => zoomTransform.applyY(d.source.y))
				.attr("x2", d => zoomTransform.applyX(d.target.x))
				.attr("y2", d => zoomTransform.applyY(d.target.y))
				.attr("stroke-width", PORT_EDGE_BASE_STROKE_WIDTH) // We decided that port edges should not get thicker when zoomed
		},
		ellipseLabelTextIfNeeded: function(labelText) {
			if (labelText.length <= LABELS_MAX_CHARS)
				return labelText
			else
				return labelText.slice(0, LABELS_MAX_CHARS - 3) + "..."
		},
		resetZoom: function() {
			this.svg
				.transition()
				.duration(750)
				.call(this.zoomBehaviour.transform, d3.zoomIdentity)
		},
		colorVertices: function() {
			this.circles
				.attr("fill", d => d.type === VertexType.PATTERN_VERTEX ? this.vertexColorFromLabels(d.labels) : "white")
				.attr("stroke", "black")
				.attr("stroke-width", d => d.type === VertexType.PATTERN_VERTEX ? 0 : 1)
		},
		colorEdges: function() {
			this.patternEdgeLines
				.attr("stroke", d => this.edgeColorFromLabel(d.label))
			if (this.patternEdgeArrows !== null)
				this.patternEdgeArrows.attr("stroke", d => this.edgeColorFromLabel(d.label))
		},
		/**
		 * When the user presses the start/stop simulation button
		 */
		onToggleSimulationStatus: function() {
			if (this.askConfirmStart) // We are asking the user if they want to start a simulation that may be too big
			{
				// The user clicked the button: they are saying that they do want to start the simulation
				this.askConfirmStart = false
				this.plot()
			} else {
				// The user clicked the button to stop/restart a running simulation
				if (!this.forceSimulation) // But there is no simulation (should not happen, but just in case)
					return

				if (this.simulationStarted) // The simulation was running
					this.forceSimulation.stop()
				else // The simulation was stopped
					D3Utils.reIgniteSimulation(this.forceSimulation)
				this.simulationStarted = !this.simulationStarted
			}
		},
		vertexHoverEnter: function(event, d) {
			let text
			if (d.type === VertexType.PATTERN_VERTEX) {
				text = `P${this.filteredEmbeddings[d.embedding].pattern + 1}, v${d.patternVertex}`
				if (d.labels.length === 0)
					text += ", No label"
				else if (d.labels.length === 1)
					text += ", Label: "
				else
					text += ", Labels: "
				text += d.labels.join(", ")

				this.hoveredEmbedding = d.embedding
				this.plotChangingAttributes() // So that elements of the hovered embedding can update their visuals
			} else if (d.type === VertexType.PORT) {
				text = `Data vertex ${d.dataVertex}`
				d3.select(event.currentTarget)
					.attr("fill", "#999")
			}
			this.setHoverText(text)
		},
		vertexHoverExit: function(event, d) {
			if (d.type === VertexType.PATTERN_VERTEX) {
				this.hoveredEmbedding = null
			} else if (d.type === VertexType.PORT) {
				d3.select(event.currentTarget)
					.attr("fill", "white")
			}
			this.removeHoverText()
		},
		edgeHoverEnter: function(_event, d) {
			this.setHoverText(`P${this.filteredEmbeddings[d.embedding].pattern + 1}, ${d.label}`)
			this.hoveredEmbedding = d.embedding
		},
		edgeHoverExit: function() {
			this.hoveredEmbedding = null
			this.removeHoverText()
		},
		setHoverText: function(text) {
			if (!this.hoverTextGroup)
				this.hoverTextGroup = this.svg.append("g")
			this.hoverTextGroup.selectAll("text").remove()
			const hoverText = this.hoverTextGroup.append("text") // Creating the top text
				.text(text)
				.attr("x", 10)
				.attr("y", 30)
			hoverText.clone(true) // Creating bottom text
				.attr("y", this.height - 30)
		},
		removeHoverText: function() {
			if (this.hoverTextGroup)
				this.hoverTextGroup.selectAll("text").remove()
			this.hoveredEmbedding = null
		},
		onVertexDoubleClick: function(d) {
			if (d.type === VertexType.PATTERN_VERTEX) {
				d3.event.stopPropagation() // So that the zoom behaviour is not activated
				this.$emit("pattern-selected", this.filteredEmbeddings[d.embedding].pattern)
			}
		},
	},
	watch: {
		// Vue can not understand alone that a new plot is needed whenever the list of embeddings changes.
		filteredEmbeddings: function() {
			this.plotIfNotTooBig()
		},
		vertexRepulsionStrength: function() {
			if (this.forceSimulation != null) {
				this.forceSimulation.force("vertexRepulsive", this.vertexRepulsiveForce)
				D3Utils.reIgniteSimulation(this.forceSimulation)
			}
		},
		hoveredEmbedding: function() {
			this.plotChangingAttributes()
		},
		showVertexNumbers: function() {
			if (this.vertexNumbers)
				this.plotChangingAttributes()
		},
		showLabels: function() {
			if (this.labelGroup)
				this.plotChangingAttributes()
		},
	},
	mounted() {
		this.plotIfNotTooBig()
	},
}
</script>

<style scoped lang="scss">
@import "bootstrap/scss/bootstrap";

.repulsion-control {
	display: inline-block;
	label {
		@extend .form-label;
	}
	input {
		@extend .form-control;
		max-width: 100px;
		display: inline-block;
		margin-left: 1em;
		margin-right: 1.5em;
	}
}

.checkboxes {
	display: inline-block;
	 >div {
		 @extend .form-check;
		 @extend .form-check-inline;

		 label {
			 @extend .form-check-label;
		 }

		 input {
			 @extend .form-check-input;
		 }
	 }
}

.buttons {
	@extend .mt-3;

	.simulation-toggle {
		@extend .btn;


		&.start {
			@extend .btn-info;
		}

		&.stop {
			@extend .btn-outline-dark;
		}
	}

	.zoom-reset {
		@extend .btn;
		@extend .btn-info;

		margin-left: 0.5em;
	}
}

.ask-confirm-start {
	color: $danger;
	font-style: italic;
	padding: 0.2em;
}

.ports-help-text {
	font-style: italic;
	font-weight: lighter;
}

</style>
