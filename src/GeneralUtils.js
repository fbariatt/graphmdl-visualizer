export default {
	/**
	 * Uses {@link FileReader} to get the text of a file
	 * @param blob The file to read, as a Blob
	 * @return A promise that resolves when the upload finishes, either with the file's content or with an error.
	 */
	uploadFileAsText: function (blob) {
		return new Promise((resolve, reject) => {
			let reader = new FileReader()
			reader.onload = event => resolve(event.target.result)
			reader.onerror = _event => reject(new Error("Upload error")) // We fire a generic error because they tend not to be very useful
			reader.readAsText(blob)
		})
	},

	/**
	 * If the given value is null, undefined or NaN, return a default value.
	 */
	defaultIfInvalid: function(value, defaultValue = null) {
		if (value === undefined || value === null || Number.isNaN(value))
			return defaultValue
		return value
	},

	/**
	 * If a value is associated to a key in a map, return it otherwise compute it.
	 * @param map A {@link Map}
	 * @param key The key to search
	 * @param mappingFunction The function to compute the new value to put into the map if one is absent.
	 * The key is passed to it as argument when used.
	 */
	mapGetOrComputeIfAbsent: function(map, key, mappingFunction) {
		if (map.has(key)) {
			return map.get(key)
		} else {
			const value = mappingFunction(key)
			map.set(key, value)
			return value
		}
	},
}
