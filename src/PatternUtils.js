import GeneralUtils from "./GeneralUtils"

export default {
	vertexCount: function (pattern) {
		return pattern["structure"]["vertices"].length
	},
	edgeCount: function(pattern) {
		return pattern["structure"]["edges"].length
	},
	labelCount: function(pattern) {
		return pattern["structure"]["vertices"].reduce((sum, vertexLabels) => sum + vertexLabels.length, 0) + this.edgeCount(pattern)
	},
	portCount: function(pattern) {
		return Object.keys(pattern["ports"]).length
	},
	/**
	 * Number of embeddings of the given pattern that are used in the data encoding
	 */
	usedEmbeddingsCount(pattern) {
		return pattern["used_embeddings"].length
	},
	/**
	 * Number of embeddings of a pattern in the data, but not only the ones used in the data encoding
	 */
	totalEmbeddingsCount: function(pattern) {
		return pattern["embeddings"].length
	},
	isPort: function(pattern, vertex) {
		return vertex in pattern["ports"]
	},
	/**
	 * Given an array of edge objects (each having a source and a target), return a map where those objects are grouped in
	 * arrays based on their source and target.
	 * Edges that are between the same pair of vertices (independently of direction) will be in the same array.
	 */
	groupEdgesByEnds: function(edges) {
		const edgesGroupedByEnds = new Map()
		edges.forEach(edge => {
			const ends = [edge.source, edge.target]
				.sort((a, b) => a - b) // So that direction of the edge is not important
				.toString() // We need to convert to string since maps do not work well with array keys
			GeneralUtils.mapGetOrComputeIfAbsent(edgesGroupedByEnds, ends, () => [])
				.push(edge)
		})
		return edgesGroupedByEnds
	},
	/**
	 * @return {Set} The data vertices to which the given pattern's used embeddings map to.
	 */
	dataVerticesUsedByPattern: function(pattern) {
		return pattern["used_embeddings"]
			.flatMap(embedding => embedding["mapping"])
			.reduce((s, dataVertex) => s.add(dataVertex), new Set())
	},
}
